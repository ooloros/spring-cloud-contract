package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method GET()
        url("/api/person") {
            queryParameters {
                parameter("socialSecurityId", "50001018865")
            }
        }
    }
    response {
        status NOT_FOUND()
        headers {
            contentType('application/json')
        }
        body([
                errorCode: "NOT_FOUND"
        ])
    }
}