package ee.ooloros.demohttpconsumer;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import ee.ooloros.demohttpproducer.NotFoundException;
import ee.ooloros.demohttpproducer.PersonController;
import ee.ooloros.demohttpproducer.PersonEntity;
import ee.ooloros.demohttpproducer.PersonRepository;
import io.restassured.module.mockmvc.RestAssuredMockMvc;

@SpringBootTest(classes = {PersonController.class, MockMvcAutoConfiguration.class})
abstract class BaseTestClass {
    private static final PersonEntity VALID_PERSON = PersonEntity.builder()
            .familyName("Raja")
            .givenName("Teele")
            .socialSecurityId("60001017869")
            .build();
    private static final PersonEntity INVALID_PERSON = PersonEntity.builder()
            .socialSecurityId("50001018865")
            .build();

    @MockBean
    private PersonRepository personRepository;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        RestAssuredMockMvc.mockMvc(mockMvc);
        doReturn(VALID_PERSON)
                .when(personRepository).getPersonBySocialSecurityId(VALID_PERSON.socialSecurityId());
        doThrow(NotFoundException.class)
                .when(personRepository).getPersonBySocialSecurityId(INVALID_PERSON.socialSecurityId());
    }
}
