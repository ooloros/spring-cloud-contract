package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method GET()
        url("/api/person") {
            queryParameters {
                parameter("socialSecurityId", "60001017869")
            }
        }
    }
    response {
        status OK()
        headers {
            contentType('application/json')
        }
        body([
                familyName: 'Raja',
                givenName: 'Teele',
                socialSecurityId: "60001017869"
        ])
    }
}