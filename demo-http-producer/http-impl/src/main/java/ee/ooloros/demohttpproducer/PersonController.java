package ee.ooloros.demohttpproducer;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/person")
public class PersonController {
    private final PersonRepository personService;

    @GetMapping
    public PersonDto getBySocialSecurityId(@RequestParam("socialSecurityId") String socialSecurityId) {
        PersonEntity person = personService.getPersonBySocialSecurityId(socialSecurityId);
        return PersonDto.builder()
                .givenName(person.givenName())
                .familyName(person.familyName())
                .socialSecurityId(person.socialSecurityId())
                .build();
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public Map<String, String> handleNotFoundError() {
        return Map.of("errorCode", "NOT_FOUND");
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public void handleInternalServerError(Exception e) {
        log.error(e.getMessage(), e);
    }
}
