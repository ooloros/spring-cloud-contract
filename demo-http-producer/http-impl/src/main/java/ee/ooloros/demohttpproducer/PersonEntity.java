package ee.ooloros.demohttpproducer;


import lombok.Builder;

public record PersonEntity(
        String givenName,
        String familyName,
        String socialSecurityId
) {
    @Builder
    @SuppressWarnings("java:S6207")
    public PersonEntity {
        //workaround for Lombok Builder + Java record
    }
}
