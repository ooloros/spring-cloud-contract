package ee.ooloros.demohttpproducer;

import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(classes = {PersonController.class, MockMvcAutoConfiguration.class})
class PersonControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private PersonRepository personService;

    @Test
    void shouldReturnHttpInternalServerError() throws Exception {
        String socialSecurityId = "38903170039";
        doThrow(RuntimeException.class)
                .when(personService).getPersonBySocialSecurityId(socialSecurityId);
        mvc.perform(get("/api/person")
                        .queryParam("socialSecurityId", socialSecurityId))
                .andExpect(status().isInternalServerError());
    }
}