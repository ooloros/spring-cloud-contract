package ee.ooloros.demohttpproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoHttpProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoHttpProducerApplication.class, args);
    }

}
