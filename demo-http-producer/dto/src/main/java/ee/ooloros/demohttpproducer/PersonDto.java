package ee.ooloros.demohttpproducer;

import lombok.Builder;

// usable for other clients
public record PersonDto(
        String givenName,
        String familyName,
        String socialSecurityId
) {
    @Builder
    public PersonDto {

    }
}
