package ee.ooloros.demohttpconsumer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.DefaultMessagePropertiesConverter;
import org.springframework.amqp.rabbit.support.MessagePropertiesConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

import ee.ooloros.demorabbitproducer.PersonListener;
import ee.ooloros.demorabbitproducer.PersonRepository;
import ee.ooloros.demorabbitproducer.config.RabbitConfiguration;

@SpringBootTest(
        classes = {PersonListener.class, BaseTestClass.MockRabbitConfiguration.class},
        properties = "spring.main.allow-bean-definition-overriding=true"
)
@DirtiesContext
public class BaseTestClass {
    private static final Map<String, String> VALID_PERSON =  Map.of(
            "familyName", "Raja",
            "givenName", "Teele",
            "socialSecurityId", "60001017869"
    );

    @MockBean
    private PersonRepository personRepository;

    @BeforeEach
    void setup() {
        doReturn(VALID_PERSON).when(personRepository).getBySocialSecurityId(VALID_PERSON.get("socialSecurityId"));
    }

    @Configuration
    @Import(RabbitConfiguration.class)
    @AutoConfigureMessageVerifier
    public static class MockRabbitConfiguration {
        private final MessagePropertiesConverter propertiesConverter = new DefaultMessagePropertiesConverter();

        @Autowired
        @ConditionalOnProperty(name = "stubrunner.amqp.mockConnection", havingValue = "true")
        // https://github.com/spring-cloud/spring-cloud-contract/issues/1706
        void setupDirectReply(ConnectionFactory connectionFactory, RabbitTemplate rabbitTemplate) throws IOException {
            @SuppressWarnings("java:S2095") // it's a mock
            Channel channel = connectionFactory.createConnection().createChannel(true);
            doAnswer(invocation -> {
                MessageProperties properties = propertiesConverter.toMessageProperties(invocation.getArgument(3), null, StandardCharsets.UTF_8.name());
                if (properties.getHeader("x-loop-prevent") == Boolean.TRUE) {
                    return null;
                } else {
                    properties.setHeader("x-loop-prevent", Boolean.TRUE);
                }
                Message message = rabbitTemplate.getMessageConverter().toMessage(
                        invocation.getArgument(4),
                        properties);
                rabbitTemplate.send("", invocation.getArgument(1), message);
                return null;
            }).when(channel).basicPublish(eq(""), any(String.class), any(Boolean.class), any(AMQP.BasicProperties.class), any(byte[].class));
        }
    }
}

