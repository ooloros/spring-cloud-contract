package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    label("sendAndReceiveEvent")
    input {
        messageFrom('rpc-exchange')
        messageHeaders {
            header("amqp_replyTo", "amq.rabbitmq.reply-to")
            header("amqp_receivedRoutingKey", "get-person-by-social-security-id")
            header("contentType", "application/json")
        }
        messageBody([
                socialSecurityId: '60001017869'
        ])
    }
    outputMessage {
        // DirectReply
        sentTo('')
        headers {
            header("contentType", "application/json")
            header("amqp_receivedRoutingKey", value(
                    consumer('{{{input.headers.amqp_replyTo.[0]}}}'),
                    producer("amq.rabbitmq.reply-to")
            ))
        }
        body([
                familyName      : 'Raja',
                givenName       : 'Teele',
                socialSecurityId: "60001017869"
        ])
    }
    // SpringAmqpStubMessages is not copying values from headers to properties - workaround
    // https://github.com/spring-cloud/spring-cloud-contract/issues/1706
    metadata([
            amqp: [
                    input: [
                            messageProperties: [
                                    replyTo           : "amq.rabbitmq.reply-to",
                                    receivedRoutingKey: "get-person-by-social-security-id"
                            ]
                    ]
            ]
    ])
}