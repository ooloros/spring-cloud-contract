package ee.ooloros.demorabbitproducer;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ee.ooloros.demorabbitproducer.config.RabbitConfiguration;

@Configuration
@Import({RabbitConfiguration.class})
public class TestConfiguration {
}
