package ee.ooloros.demorabbitproducer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.stubrunner.StubFinder;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;

import java.util.Map;

import ee.ooloros.demorabbitproducer.config.ApiConfiguration;
import ee.ooloros.demorabbitproducer.config.ApiConfiguration.ApiProperties;

@AutoConfigureStubRunner(
        ids = {"ee.ooloros.demohttppublisher:demo-rabbit-consumer:+:stubs"},
        stubsMode = StubRunnerProperties.StubsMode.CLASSPATH
)
@SpringBootTest(classes = {PersonRepository.class, ApiConfiguration.class})
@AutoConfigureWebMvc
@AutoConfigureMockMvc
class PersonRepositoryTest {
    private static final Map<String, Object> VALID_PERSON =  Map.of(
            "familyName", "Raja",
            "givenName", "Teele",
            "socialSecurityId", "60001017869"
    );

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private StubFinder stubFinder;
    @MockBean
    private ApiProperties apiProperties;

    @BeforeEach
    void setup() {
        int port = stubFinder.findStubUrl("ee.ooloros.demohttppublisher", "demo-rabbit-consumer").getPort();
        doReturn(String.format("http://localhost:%d/", port)).when(apiProperties).getBaseUrl();
    }

    @Test
    void shouldPass() {
        Map<String, Object> result = personRepository.getBySocialSecurityId(VALID_PERSON.get("socialSecurityId").toString());
        assertEquals(VALID_PERSON, result);
    }
}