package ee.ooloros.demorabbitproducer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.Map;

@SpringBootTest
@ActiveProfiles("rabbitmq")
@ContextConfiguration(
        initializers = {RabbitMqInitializer.class},
        classes = {TestConfiguration.class, PersonListener.class}
)
class PersonListenerTest {
    @MockBean
    private PersonRepository personRepository;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    void shouldPass() {
        //given
        var request = Map.of("socialSecurityId", "60001017869");
        var expected = Map.of(
                "familyName", "Raja",
                "givenName", "Teele",
                "socialSecurityId", request.get("socialSecurityId")
        );
        //when
        doReturn(expected).when(personRepository).getBySocialSecurityId(request.get("socialSecurityId"));
        //then
        Map<String, Object> result = rabbitTemplate.convertSendAndReceiveAsType(
                "rpc-exchange",
                "get-person-by-social-security-id",
                request,
                new ParameterizedTypeReference<>() {
                });
        assertEquals(expected, result);
    }
}