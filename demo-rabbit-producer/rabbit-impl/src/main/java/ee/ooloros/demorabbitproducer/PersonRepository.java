package ee.ooloros.demorabbitproducer;

import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import ee.ooloros.demorabbitproducer.config.ApiConfiguration.ApiProperties;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class PersonRepository {
    private final RestTemplate restTemplate;
    private final ApiProperties apiProperties;

    @SuppressWarnings("unchecked")
    public Map<String, Object> getBySocialSecurityId(String socialSecurityId) {
        return restTemplate.getForObject(
                apiProperties.getBaseUrl() + "/api/person?socialSecurityId={socialSecurityId}",
                Map.class,
                socialSecurityId);
    }
}
