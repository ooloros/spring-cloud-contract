package ee.ooloros.demorabbitproducer;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Map;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class PersonListener {
    private static final String QUEUE_NAME = "x-service.get-person-by-social-security-id-queue";
    private static final String ROUTING_KEY = "get-person-by-social-security-id";
    private final PersonRepository personRepository;

    @Bean
    public DirectExchange rpcExchange() {
        return new DirectExchange("rpc-exchange");
    }

    @Bean
    public Queue queue() {
        return QueueBuilder.durable(QUEUE_NAME)
                .build();
    }

    @Bean
    public Binding binding(Queue queue, DirectExchange rpcExchange) {
        return BindingBuilder.bind(queue).to(rpcExchange).with(ROUTING_KEY);
    }

    @RabbitListener(queues = QUEUE_NAME)
    public Map<String, Object> handle(Map<String, String> request) {
        return personRepository.getBySocialSecurityId(request.get("socialSecurityId"));
    }

}
