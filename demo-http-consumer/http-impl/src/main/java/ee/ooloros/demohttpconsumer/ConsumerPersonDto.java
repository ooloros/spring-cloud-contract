package ee.ooloros.demohttpconsumer;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

public record ConsumerPersonDto(@JsonUnwrapped ee.ooloros.demohttpproducer.PersonDto personEntity) {}
