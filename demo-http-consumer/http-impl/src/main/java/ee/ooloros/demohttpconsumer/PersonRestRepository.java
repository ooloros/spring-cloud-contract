package ee.ooloros.demohttpconsumer;

import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import ee.ooloros.demohttpconsumer.ApiConfiguration.ApiProperties;
import ee.ooloros.demohttpproducer.PersonDto;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class PersonRestRepository {
    private final RestTemplate restTemplate;
    private final ApiProperties apiProperties;

    public PersonDto getPersonBySocialSecurityId(String socialSecurityId) {
        return restTemplate.getForObject(
                apiProperties.getBaseUrl() + "/api/person?socialSecurityId={socialSecurityId}",
                PersonDto.class,
                socialSecurityId);
    }
}
