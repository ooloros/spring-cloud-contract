package ee.ooloros.demohttpconsumer;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import ee.ooloros.demohttpproducer.PersonDto;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/person")
public class PersonController {
    private final PersonRestRepository personService;

    @GetMapping
    public ConsumerPersonDto getBySocialSecurityId(@RequestParam("socialSecurityId") String socialSecurityId) {
        PersonDto person = personService.getPersonBySocialSecurityId(socialSecurityId);
        return new ConsumerPersonDto(person);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(HttpClientErrorException.NotFound.class)
    public void handleNotFoundError() {
        // nothing, but TO-DO: return errorDto
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public void handleInternalServerError() {
        // nothing, but TO-DO: return errorDto
    }
}
