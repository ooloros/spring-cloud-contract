package ee.ooloros.demohttpconsumer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;

import java.util.Map;

import ee.ooloros.demohttpproducer.PersonDto;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class PersonRabbitRepository {
    private final RabbitTemplate rabbitTemplate;

    public PersonDto getPersonBySocialSecurityId(String socialSecurityId) {
        var request = Map.of("socialSecurityId", socialSecurityId);
        return rabbitTemplate.convertSendAndReceiveAsType(
                "rpc-exchange",
                "get-person-by-social-security-id",
                request,
                new ParameterizedTypeReference<>() {
                });
    }
}
