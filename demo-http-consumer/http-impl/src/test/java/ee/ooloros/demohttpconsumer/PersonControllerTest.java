package ee.ooloros.demohttpconsumer;

import static ee.ooloros.demohttpconsumer.TestUtils.INVALID_PERSON;
import static ee.ooloros.demohttpconsumer.TestUtils.VALID_PERSON;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;

@SpringBootTest(classes = PersonController.class)
@AutoConfigureWebMvc
@AutoConfigureMockMvc
class PersonControllerTest {

    @Autowired(required = false)
    private MockMvc mvc;
    @MockBean
    private PersonRestRepository personService;

    @BeforeEach
    void setup() {
        doReturn(VALID_PERSON).when(personService).getPersonBySocialSecurityId(VALID_PERSON.getSocialSecurityId());
    }

    @Test
    void shouldReturnHttpOk() throws Exception {
        mvc.perform(get("/api/person")
                        .queryParam("socialSecurityId", VALID_PERSON.getSocialSecurityId()))
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturnContent() throws Exception {
        mvc.perform(get("/api/person")
                        .queryParam("socialSecurityId", VALID_PERSON.getSocialSecurityId()))
                .andExpect(content().json("""
                        { "givenName": "%s", "familyName": "%s", "socialSecurityId": "%s" }
                        """
                        .formatted(
                                VALID_PERSON.getGivenName(),
                                VALID_PERSON.getFamilyName(),
                                VALID_PERSON.getSocialSecurityId()
                        )));
    }

    @Test
    void shouldReturnHttpInternalServerError() throws Exception {
        doThrow(RuntimeException.class)
                .when(personService).getPersonBySocialSecurityId(INVALID_PERSON.getSocialSecurityId());
        mvc.perform(get("/api/person")
                        .queryParam("socialSecurityId", INVALID_PERSON.getSocialSecurityId()))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void shouldReturnHttpNotFound() throws Exception {
        doThrow(HttpClientErrorException.NotFound.class)
                .when(personService).getPersonBySocialSecurityId(INVALID_PERSON.getSocialSecurityId());
        mvc.perform(get("/api/person")
                        .queryParam("socialSecurityId", INVALID_PERSON.getSocialSecurityId()))
                .andExpect(status().isNotFound());
    }
}