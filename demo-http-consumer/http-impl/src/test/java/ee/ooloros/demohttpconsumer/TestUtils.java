package ee.ooloros.demohttpconsumer;

import ee.ooloros.demohttpproducer.PersonDto;

public class TestUtils {
    public static final PersonDto VALID_PERSON = PersonDto.builder()
            .familyName("Raja")
            .givenName("Teele")
            .socialSecurityId("60001017869")
            .build();
    public static final PersonDto INVALID_PERSON = PersonDto.builder()
            .socialSecurityId("50001018865")
            .build();
}
