package ee.ooloros.demohttpconsumer;

import static ee.ooloros.demohttpconsumer.TestUtils.INVALID_PERSON;
import static ee.ooloros.demohttpconsumer.TestUtils.VALID_PERSON;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.stubrunner.StubFinder;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.web.client.HttpClientErrorException;

import ee.ooloros.demohttpconsumer.ApiConfiguration.ApiProperties;
import ee.ooloros.demohttpproducer.PersonDto;

@AutoConfigureStubRunner(
        ids = {"ee.ooloros.demohttppublisher:demo-http-consumer:+:stubs"},
        stubsMode = StubRunnerProperties.StubsMode.CLASSPATH
)
@SpringBootTest(classes = {PersonRestRepository.class, ApiConfiguration.class})
@EnableAutoConfiguration
class PersonRestRepositoryTest {

    @Autowired
    private PersonRestRepository personRepository;
    @Autowired
    private StubFinder stubFinder;
    @MockBean
    private ApiProperties apiProperties;

    @BeforeEach
    void setup() {
        int port = stubFinder.findStubUrl("ee.ooloros.demohttppublisher", "demo-http-consumer").getPort();
        doReturn(String.format("http://localhost:%d/", port)).when(apiProperties).getBaseUrl();
    }

    @Test
    void shouldPass() {
        PersonDto result = personRepository.getPersonBySocialSecurityId(VALID_PERSON.getSocialSecurityId());
        assertEquals(VALID_PERSON, result);
    }

    @Test
    void shouldFail() {
        assertThrows(HttpClientErrorException.NotFound.class,
                () -> personRepository.getPersonBySocialSecurityId(INVALID_PERSON.getSocialSecurityId()));
    }
}