
package ee.ooloros.demohttpconsumer;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextClosedEvent;
import org.testcontainers.containers.RabbitMQContainer;

import java.util.Map;

public class RabbitMqInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    static RabbitMQContainer container;

    @Override
    public void initialize(@NotNull ConfigurableApplicationContext context) {
        container = new RabbitMQContainer("rabbitmq:latest")
                .withUser("quest", "quest");
        container.start();

        context.addApplicationListener(applicationEvent -> {
            if (applicationEvent instanceof ContextClosedEvent) {
                container.stop();
            }
        });
        TestPropertyValues.of(
                        Map.of("spring.rabbitmq.host", container.getHost(),
                                "spring.rabbitmq.port", container.getAmqpPort().toString(),
                                "spring.rabbitmq.username", container.getAdminUsername(),
                                "spring.rabbitmq.password", container.getAdminPassword()))
                .applyTo(context);
    }
}
