package ee.ooloros.demohttpconsumer;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextClosedEvent;

import java.util.Map;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.common.Slf4jNotifier;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;

public class WireMockInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        WireMockServer wireMockServer = new WireMockServer(new WireMockConfiguration()
                        .dynamicPort()
                        .notifier(new Slf4jNotifier(true)));
        wireMockServer.start();

        configurableApplicationContext
                        .getBeanFactory()
                        .registerSingleton("someApiWireMockServer", wireMockServer);

        configurableApplicationContext.addApplicationListener(applicationEvent -> {
            if (applicationEvent instanceof ContextClosedEvent) {
                wireMockServer.stop();
            }
        });

        TestPropertyValues
                        .of(Map.of("some.api.host",
                                        String.format("http://localhost:%d/", wireMockServer.port())))
                        .applyTo(configurableApplicationContext);
    }
}
