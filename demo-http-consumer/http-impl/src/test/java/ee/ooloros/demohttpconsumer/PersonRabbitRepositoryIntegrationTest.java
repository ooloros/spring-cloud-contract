package ee.ooloros.demohttpconsumer;

import static ee.ooloros.demohttpconsumer.TestUtils.VALID_PERSON;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.Map;

import ee.ooloros.demohttpproducer.PersonDto;

@SpringBootTest
@ContextConfiguration(
        initializers = {RabbitMqInitializer.class},
        classes = {PersonRabbitRepository.class, PersonRabbitRepositoryIntegrationTest.MockRabbitConfiguration.class}
)
@ActiveProfiles("rabbitmq")
class PersonRabbitRepositoryIntegrationTest {
    @MockBean
    private MockRepository mockRepository;

    @Autowired
    private PersonRabbitRepository personRabbitRepository;

    @Test
    void shouldPass() {
        doReturn(VALID_PERSON).when(mockRepository).getBySocialSecurityId(VALID_PERSON.getSocialSecurityId());
        PersonDto result = personRabbitRepository.getPersonBySocialSecurityId(VALID_PERSON.getSocialSecurityId());
        assertEquals(VALID_PERSON, result);
    }

    @Configuration
    @Import({RabbitConfiguration.class, MockRepository.class})
    public static class MockRabbitConfiguration {
        private static final String QUEUE_NAME = "x-service.get-person-by-social-security-id-queue";
        private static final String ROUTING_KEY = "get-person-by-social-security-id";
        private final MockRepository mockRepository;

        public MockRabbitConfiguration(MockRepository mockRepository) {
            this.mockRepository = mockRepository;
        }

        @Bean
        public DirectExchange rpcExchange() {
            return new DirectExchange("rpc-exchange");
        }

        @Bean
        public Queue queue() {
            return QueueBuilder.durable(QUEUE_NAME)
                    .build();
        }

        @Bean
        public Binding binding(Queue queue, DirectExchange rpcExchange) {
            return BindingBuilder.bind(queue).to(rpcExchange).with(ROUTING_KEY);
        }

        @RabbitListener(queues = QUEUE_NAME)
        public PersonDto handle(Map<String, String> request) {
            return mockRepository.getBySocialSecurityId(request.get("socialSecurityId"));
        }
    }

    @Repository
    public static class MockRepository {
        @SuppressWarnings("unchecked")
        public PersonDto getBySocialSecurityId(String socialSecurityId) {
            throw new UnsupportedOperationException();
        }
    }
}