package ee.ooloros.demohttpconsumer;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static ee.ooloros.demohttpconsumer.TestUtils.INVALID_PERSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;

@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(
        initializers = WireMockInitializer.class,
        classes = PersonControllerIntegrationTest.TestConfiguration.class
)
class PersonControllerIntegrationTest {
    @Autowired(required = false)
    private MockMvc mvc;
    @Autowired(required = false)
    private WireMockServer wireMockServer;

    @Test
    void shouldFailWithHttp404() throws Exception {
        //when
        wireMockServer.stubFor(WireMock.get(urlPathMatching("/api/person"))
                .withQueryParam("socialSecurityId", equalTo(INVALID_PERSON.getSocialSecurityId()))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.NOT_FOUND.value())));
        //then
        mvc.perform(get("/api/person")
                        .queryParam("socialSecurityId", INVALID_PERSON.getSocialSecurityId()))
                .andExpect(status().isNotFound());
    }

    @Configuration
    @ComponentScan(basePackages = "ee.ooloros.demohttpconsumer")
    public static class TestConfiguration {

    }
}
