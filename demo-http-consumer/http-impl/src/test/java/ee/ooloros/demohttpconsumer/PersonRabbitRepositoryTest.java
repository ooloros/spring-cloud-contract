package ee.ooloros.demohttpconsumer;

import static ee.ooloros.demohttpconsumer.TestUtils.VALID_PERSON;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.server.EnableStubRunnerServer;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import ee.ooloros.amqpstubsrunner.AmqpStubsWrapper;
import ee.ooloros.demohttpproducer.PersonDto;

@AutoConfigureStubRunner(
        ids = {"ee.ooloros.demorabbitpublisher:demo-http-consumer:+:stubs"},
        stubsMode = StubRunnerProperties.StubsMode.CLASSPATH
)
@SpringBootTest(
        classes = {PersonRabbitRepository.class, PersonRabbitRepositoryTest.MockRabbitConfiguration.class},
        properties = {
                "stubrunner.amqp.enabled=true",
                "stubrunner.stream.enabled=false"
        }
)
@EnableStubRunnerServer
@ActiveProfiles("rabbitmq")
@AutoConfigureWebMvc
@AutoConfigureMockMvc
class PersonRabbitRepositoryTest {

    @Autowired
    private PersonRabbitRepository personRabbitRepository;
/*
    @BeforeEach
    void setup() {
        int port = stubFinder.findStubUrl("ee.ooloros.demohttppublisher", "demo-http-consumer").getPort();
    }
*/
    @Test
    void shouldPass() {
        PersonDto result = personRabbitRepository.getPersonBySocialSecurityId(VALID_PERSON.getSocialSecurityId());
        assertEquals(VALID_PERSON, result);
    }
/*
    @Test
    void shouldFail() {
        assertThrows(HttpClientErrorException.NotFound.class,
                () -> personRepository.getPersonBySocialSecurityId(INVALID_PERSON.getSocialSecurityId()));
    }*/

    @Configuration
    @Import({RabbitConfiguration.class, AmqpStubsWrapper.class})
    @AutoConfigureMessageVerifier
    public static class MockRabbitConfiguration {

    }
}