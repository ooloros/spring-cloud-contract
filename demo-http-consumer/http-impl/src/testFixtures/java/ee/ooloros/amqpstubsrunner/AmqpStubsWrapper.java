package ee.ooloros.amqpstubsrunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.command.ActiveMQObjectMessage;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.cloud.contract.stubrunner.messaging.jms.StubRunnerJmsConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.AbstractMessageListenerContainer;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

import lombok.Builder;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class AmqpStubsWrapper {
    @Configuration
    public static class JmsMockConfiguration {
        @Bean
        public javax.jms.ConnectionFactory jmsConnectionFactory() throws JMSException {
            final var mockConnection = mock(Connection.class);
            final var connectionFactory = mock(javax.jms.ConnectionFactory.class);
            final var session = mock(ActiveMQSession.class);
            final var messageProducer = mock(MessageProducer.class);
            doReturn(mockConnection).when(connectionFactory).createConnection();
            doReturn(session).when(mockConnection).createSession(any(Boolean.class), any(Integer.class));
            doCallRealMethod().when(session).createTextMessage(any(String.class));
            doReturn(messageProducer).when(session).createProducer(null);
            return connectionFactory;
        }

        @Bean
        public JmsTemplate jmsTemplate(javax.jms.ConnectionFactory jmsConnectionFactory) {
            JmsTemplate jmsTemplate = new JmsTemplate();
            jmsTemplate.setConnectionFactory(jmsConnectionFactory);
            return jmsTemplate;
        }
    }

    @Component
    @Import(StubRunnerJmsConfiguration.class)
    @RequiredArgsConstructor
    public static class StubsConfiguration {
        private final Jackson2JsonMessageConverter messageConverter = new Jackson2JsonMessageConverter();
        private final RabbitTemplate rabbitTemplate;
        private final List<AbstractMessageListenerContainer> containers;
        private final ConnectionFactory connectionFactory;
        private final javax.jms.ConnectionFactory jmsConnectionFactory;

        @PostConstruct
        private void setupChannelMock() throws IOException {
            @SuppressWarnings({"java:S1854", "java:S2095"})
            Channel channel = connectionFactory.createConnection().createChannel(true);
            doReturn(true).when(channel).isOpen();
            doAnswer(inv -> handleMessage(Args.builder()
                    .exchangeName(inv.getArgument(0))
                    .routingKey(inv.getArgument(1))
                    .properties(inv.getArgument(3))
                    .body(inv.getArgument(4))
                    .build())
            ).when(channel).basicPublish(any(String.class), any(String.class), any(Boolean.class), any(AMQP.BasicProperties.class), any(byte[].class));
        }

        private Void handleMessage(Args args) throws JMSException, ExecutionException, InterruptedException, JsonProcessingException {
            var stubContainer = containers.stream()
                    .filter(c -> Objects.equals(c.getDestinationName(), args.exchangeName))
                    .findFirst().orElse(null);
            if (stubContainer == null || stubContainer.getMessageListener() == null) {
                return null;
            }
            var messageListener = (MessageListener) stubContainer.getMessageListener();
            Message input = convert(args.body, args.properties, args.routingKey);
            if (args.properties.getReplyTo() == null) {
                messageListener.onMessage(input);
            } else {
                handleSync(args.channel, messageListener, input);
            }
            return null;
        }

        private void handleSync(Channel channel, MessageListener listener, Message message) throws JMSException, ExecutionException, InterruptedException, JsonProcessingException {
            MessageProducer producer = jmsConnectionFactory.createConnection()
                    .createSession(false, Session.AUTO_ACKNOWLEDGE)
                    .createProducer(null);
            CompletableFuture<?> reply = new CompletableFuture<>();
            doAnswer(inv2 -> {
                reply.complete(inv2.getArgument(0));
                return null;
            }).when(producer).send(any(Message.class));

            listener.onMessage(message);
            rabbitTemplate.onMessage(convert((TextMessage) reply.get()), channel);
        }

        private Message convert(byte[] body, AMQP.BasicProperties properties, String routingKey) throws JMSException {
            var message = new ActiveMQObjectMessage();
            message.setObject(body);
            message.setStringProperty("amqp_replyTo", properties.getReplyTo());
            message.setStringProperty("amqp_receivedRoutingKey", routingKey);
            message.setStringProperty("contentType", properties.getContentType());
            return message;
        }

        private org.springframework.amqp.core.Message convert(TextMessage message) throws JMSException, JsonProcessingException {
            MessageProperties messageProperties = new MessageProperties();
            messageProperties.setContentType(message.getStringProperty("contentType"));
            messageProperties.setHeader("amqp_receivedRoutingKey", message.getStringProperty("amqp_receivedRoutingKey"));
            messageProperties.setReceivedRoutingKey(message.getStringProperty("amqp_receivedRoutingKey"));
            messageProperties.setCorrelationId("1");
            return messageConverter.toMessage(new ObjectMapper().readValue(message.getText(), Map.class), messageProperties);
        }

        private record Args(
                Channel channel,
                String exchangeName,
                String routingKey,
                AMQP.BasicProperties properties,
                byte[] body
        ) {
            @Builder
            public Args {

            }
        }
    }
}
