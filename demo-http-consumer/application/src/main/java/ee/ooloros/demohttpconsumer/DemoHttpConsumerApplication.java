package ee.ooloros.demohttpconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoHttpConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoHttpConsumerApplication.class, args);
    }

}
